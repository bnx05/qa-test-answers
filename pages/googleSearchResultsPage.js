'use strict';

const EC = protractor.ExpectedConditions

const SearchResultsPage = function() {

  this.firstPageResults = function() {
    return element.all(by.css('.iUh30'))
  };

  this.clickDesiredResult = function(desiredUrl) {
    this.firstPageResults().map(function(el) {
      return el.getText();
    }).then(function(resultUrls) {
      let idx = resultUrls.indexOf(desiredUrl);
      if (idx > -1) {
        let correctLink = element(by.xpath(`(//cite[contains(@class, "iUh30")])[${idx + 1}]`));
        browser.wait(EC.elementToBeClickable(correctLink));
        correctLink.click();
      } else {
        throw new Error(`Desired URL not found in the first page! URLs found: ${resultUrls}`)
      }
    })
  };

};

module.exports = SearchResultsPage;