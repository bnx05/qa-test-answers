'use strict';

const EC = protractor.ExpectedConditions;

const watchVideoPage = function() {
  this.upNextVideo = element(by.css('div#items[class*="secondary-results"] ytd-compact-autoplay-renderer #video-title'));
  this.videoPlayer = element(by.id('ytd-player'));
  this.videoTitle = element(by.css('h1[class*="title"]'));

  this.openUpNextVideo = function() {
    browser.wait(EC.elementToBeClickable(this.upNextVideo));
    this.upNextVideo.click();
    browser.sleep(5000);

    return new watchVideoPage();
  };
};

module.exports = watchVideoPage;