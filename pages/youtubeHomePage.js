'use strict';

const EC = protractor.ExpectedConditions
const watchVideo = require('./watchVideoPage.js')

const youtubeHomePage = function() {
  this.trendingRowLeftmostVid = element(by.css('#contents > ytd-item-section-renderer:first-of-type #items > ytd-grid-video-renderer:first-of-type #video-title'));

  this.open = function() {
    browser.waitForAngularEnabled(false);
    browser.get('https://www.youtube.com/');
  };

  this.openVideo = function(videoLocator) {
    browser.wait(EC.elementToBeClickable(videoLocator));
    videoLocator.click();
    browser.wait(EC.urlContains('watch'));

    return new watchVideo();
  }

};

module.exports = youtubeHomePage;