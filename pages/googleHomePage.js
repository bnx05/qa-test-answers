'use strict';

const searchResults = require('./googleSearchResultsPage.js');

const googleHomePage = function() {
  this.searchField = element(by.css('input[title="Search"]'));
  this.searchButton = element(by.css('.FPdoLc.VlcLAe > center > input[name="btnK"]'));

  this.open = function() {
    browser.waitForAngularEnabled(false);
    browser.get('https://www.google.com/');
  };

  this.searchForSomething = function(something) {
    this.searchField.sendKeys(something);
    this.searchButton.click();

    return new searchResults();
  };
};

module.exports = googleHomePage;