'use strict';

const EC = protractor.ExpectedConditions;

const youtubeNavbar = function() {
  this.youtubeLogo = element(by.xpath('(//a[@id="logo"])[1]'));

  this.openYoutubeHomePage = function() {
    this.youtubeLogo.click();
    browser.wait(EC.urlIs('https://www.youtube.com/'));
    browser.wait(EC.invisibilityOf($('#ytd-player')));
  };
};

module.exports = youtubeNavbar;