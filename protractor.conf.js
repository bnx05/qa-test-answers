exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub',
  directConnect: true,
  specs: [
    'googleSearchForYoutube.spec.js',
    'youtube.spec.js',
  ],

  onPrepare: function() {
    let width = 1600;
    let height = 1200;
    browser.driver.manage().window().setSize(width, height);
  },
};
