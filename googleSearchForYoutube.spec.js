const google = require('./pages/googleHomePage.js');

describe('Go to Youtube via Google search engine', function() {
  it('should do a Google search for Youtube', function() {
    let googleHome = new google();
    let youtubeUrl = 'https://www.youtube.com/';

    googleHome.open();
    let searchResults = googleHome.searchForSomething('youtube');
    searchResults.clickDesiredResult(youtubeUrl);
    expect(browser.getCurrentUrl()).toEqual(youtubeUrl)
  });
});
