const EC = protractor.ExpectedConditions;
const youtube = require('./pages/youtubeHomePage.js')
const youtubeNavbar = require('./pages/youtubeNavbar.js')

describe('Open videos on YouTube', function() {
  it('Should open first video (non-advertisement) in the home page', function() {
    let ytHome = new youtube();
    let ytNavbar = new youtubeNavbar();

    ytHome.open();
    let trendingVid = ytHome.openVideo(ytHome.trendingRowLeftmostVid);
    browser.wait(EC.elementToBeClickable(trendingVid.videoPlayer));

    let nextVideo = trendingVid.openUpNextVideo();
    browser.wait(EC.elementToBeClickable(nextVideo.videoPlayer));

    ytNavbar.openYoutubeHomePage();
    expect(browser.getTitle()).toEqual('YouTube');
  })
})